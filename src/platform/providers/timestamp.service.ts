import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';

@Injectable()
export class TimestampService {

    constructor() { }

    get timestamp() {
        return firebase.firestore.FieldValue.serverTimestamp();
    }

}
