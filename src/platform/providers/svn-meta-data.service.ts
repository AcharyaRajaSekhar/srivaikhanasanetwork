import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SvnMetaDataService {

    META_DATA_URL: string = "/assets/data/meta.data.json";

    constructor(private http: HttpClient) { }

    getFormConfigByFormName(formName): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.META_DATA_URL).subscribe(data => {
                let temp = data['form-config'];
                if (temp) resolve(temp[formName.toString()]);
                else resolve(null);
            })
        });
    }

    getServicesConfig(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.META_DATA_URL).subscribe(data => {
                resolve(data['service-config'].services.filter(x => !!x).sort((a, b) => a.order - b.order));
            })
        });
    }

    getLanguagesConfig(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.META_DATA_URL).subscribe(data => {
                resolve(data['settings-config'].languages.filter(x => !!x).sort((a, b) => a.order - b.order));
            })
        });
    }

    getStateCodes(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(this.META_DATA_URL).subscribe(data => {
                resolve(data['india-states'].states);
            })
        });
    }

}