import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TempleProfilePage } from './temple-profile';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    TempleProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(TempleProfilePage),
    PipesModule,
    ComponentsModule
  ],
})
export class TempleProfilePageModule {}
