import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { AddressApiProvider } from '../../providers/address-api/address-api';
import { EditOpenTimingsComponent } from '../../components/open-timings/edit-open-timings/edit-open-timings';
import { SvnModal80by80 } from '../../components/platform/SvnModal80by80';
import { SvnModal100by60 } from '../../components/platform/SvnModal100by60';
import { GoogleMapsComponent } from '../../components/google-maps/google-maps';
import { BackButtonEventHandler } from '@sri-vaikhanasa-network/ion-platform-services-library';

@IonicPage()
@Component({
  selector: 'page-temple-profile',
  templateUrl: 'temple-profile.html',
})
export class TempleProfilePage {

  temple: any = {};
  backButtonEventHandle: any;
  slides: Array<string> = [];
  panelName: string = "profile";

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private photoViewer: PhotoViewer,
    private addressApi: AddressApiProvider,
    private modalCtrl: ModalController,
    private backButtonEventHandler: BackButtonEventHandler) {
    this.temple = this.navParams.data;
    if (this.temple.photos && this.temple.photos.length > 0) {
      this.slides = this.temple.photos;
    } else {
      this.slides = ['assets/imgs/defaults/1.png'];
    }
  }

  getPostOfficeDetails() {
    return this.addressApi.getPostOfficeDetails(this.temple.address);
  }

  getMapsAddressDetails() {
    return this.addressApi.getMapsAddressDetails(this.temple.address);
  }

  ionViewWillEnter() {
    this.backButtonEventHandle = this.backButtonEventHandler.registerPageLevelEvent();
  }

  ionViewWillLeave() {
    this.backButtonEventHandler.unregisterPageLevelEvent(this.backButtonEventHandle);
  }

  zoom(imgURL) {
    this.photoViewer.show(imgURL, this.temple.name);
  }

  openEditTimings() {
    let m = this.modalCtrl.create(SvnModal80by80, { target: EditOpenTimingsComponent, name: "temple", value: this.temple });
    m.present();
  }

  Goto(target) {
    if (target) this.panelName = target;
  }

  openMaps() {
    let m = this.modalCtrl.create(SvnModal100by60, { target: GoogleMapsComponent, data: { latitude: '13.741967', longitude: '79.339159', title: this.temple.name, address: this.getMapsAddressDetails() } });
    m.present();
  }

  goBack() {
    this.navCtrl.pop();
  }

}
