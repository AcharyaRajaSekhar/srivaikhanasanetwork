import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { ToastMessageService, BusyIndicatorService, WindowService } from '@sri-vaikhanasa-network/ion-platform-services-library';
import * as firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-phone-login',
  templateUrl: 'phone-login.html',
})
export class PhoneLoginPage {

  countryCode: string = "+91";
  phoneNumber: string;
  otp: string;
  windowRef: any;
  @ViewChild('captchaDiv') reCaptchaDivRef: ElementRef;

  constructor(public navCtrl: NavController,
    private busyIndicator: BusyIndicatorService,
    private win: WindowService,
    private toastr: ToastMessageService) {
  }

  ngOnInit() {
    this.windowRef = this.win.windowRef;
    let tempOpt = { size: 'invisible', callback: (resp) => { console.log(resp); }, 'expired-callback': (resp) => { console.log(resp); } }
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(this.reCaptchaDivRef.nativeElement, tempOpt);
    this.windowRef.recaptchaVerifier.render();
  }

  goBack() {
    this.navCtrl.pop();
  }

  sendOtp() {
    this.busyIndicator.show();
    try {
      const appVerifier = this.windowRef.recaptchaVerifier;
      firebase.auth().signInWithPhoneNumber(this.countryCode + this.phoneNumber, appVerifier)
        .then(result => { this.windowRef.confirmationResult = result; this.busyIndicator.hide(); })
        .catch(error => { this.toastr.show(error.message, true); this.busyIndicator.hide(); });
    }
    catch (exc) {
      this.toastr.show(exc.message, true);
      this.busyIndicator.hide();
    }
  }

  verifyCode() {
    this.busyIndicator.show();
    this.windowRef.confirmationResult
      .confirm(this.otp)
      .then(result => { console.log(result.user); this.windowRef.confirmationResult = undefined; this.busyIndicator.hide(); })
      .catch(error => { console.log(error); this.toastr.show("Incorrect otp entered...", true); this.busyIndicator.hide(); });
  }

}
