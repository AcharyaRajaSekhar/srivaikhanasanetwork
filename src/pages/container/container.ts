import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Tabs, Events } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-container',
  templateUrl: 'container.html'
})
export class ContainerPage {

  @ViewChild('rootPageTabs') tabRef: Tabs;

  constructor(
    private auth: AuthService,
    private events: Events,
    public navCtrl: NavController) {

    this.events.subscribe('tabber.tabs.select', (index) => {
      if(this.tabRef && this.tabRef.length) {
        if(index > -1 && index < this.tabRef.length()) {
          this.tabRef.select(index);
        }
      }
    })

  }

  ionViewCanEnter(): Promise<boolean> {
    return this.auth.isLoggedIn();
  }

}

