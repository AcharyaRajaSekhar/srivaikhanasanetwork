import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { MySettingsRepo } from '../../providers/my-settings/my-settings.service';
import { BackButtonEventHandler } from '@sri-vaikhanasa-network/ion-platform-services-library';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(
    private backButtonHandler: BackButtonEventHandler,
    private settings: MySettingsRepo) {
    this.settings.loadMasterData();
  }

  goBack() {
    this.backButtonHandler.goBackToHome();
  }

}
