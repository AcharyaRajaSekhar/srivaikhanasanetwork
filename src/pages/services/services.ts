import { Component } from '@angular/core';
import { IonicPage, NavController, ActionSheetController } from 'ionic-angular';
import { SvnMetaDataService } from '../../platform/providers/svn-meta-data.service';
import { BackButtonEventHandler } from '@sri-vaikhanasa-network/ion-platform-services-library';

@IonicPage()
@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})
export class ServicesPage {

  services: Array<any> = [];

  constructor(private navCtrl: NavController,
    private actionSheetCtrl: ActionSheetController,
    private configService: SvnMetaDataService,
    private backButtonHandler: BackButtonEventHandler) {
  }

  ionViewWillEnter() {

    this.configService.getServicesConfig().then(s => this.services = s);

  }

  goBack() {
    this.backButtonHandler.goBackToHome();
  }

  onServiceClick(service) {
    if (service.options && service.options.length > 0) {
      let buttons = [];
      for (let option of service.options.filter(x => !!x).sort((a, b) => a.order - b.order)) {
        let button = {
          text: option.title,
          icon: option.icon,
          handler: () => {
            this.navCtrl.push(option.targetPage, option);
            return true;
          }
        }
        buttons.push(button);
      }
      let actionSheet = this.actionSheetCtrl.create({
        title: service.title,
        buttons: buttons
      });

      actionSheet.present();
    }
    else {
      this.navCtrl.push(service.targetPage, service);
    }
  }

}
