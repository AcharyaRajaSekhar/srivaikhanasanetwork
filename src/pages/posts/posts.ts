import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PostsProvider } from '../../providers/posts/posts';
import { BackButtonEventHandler } from '@sri-vaikhanasa-network/ion-platform-services-library';

@IonicPage()
@Component({
  selector: 'page-posts',
  templateUrl: 'posts.html',
})
export class PostsPage {

  pageConfig: any;
  posts: Array<any>;
  backButtonEventHandle: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private backButtonHandler: BackButtonEventHandler,
    private postsSvc: PostsProvider) {
    this.pageConfig = this.navParams.data;

  }

  ionViewWillEnter() {
    this.backButtonEventHandle = this.backButtonHandler.registerPageLevelEvent();
    this.postsSvc.getPostsByType(this.pageConfig.id).subscribe(p => this.posts = p);
  }

  ionViewWillLeave() {
    this.backButtonHandler.unregisterPageLevelEvent(this.backButtonEventHandle);
  }

  addNewPost() {
    this.navCtrl.push('NewPostPage', this.pageConfig);
  }

  goBack() {
    this.navCtrl.pop();
  }

}
