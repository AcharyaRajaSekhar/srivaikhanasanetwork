import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewPostPage } from './new-post';
import { DynamicFormsModule } from '@sri-vaikhanasa-network/ion-dynamic-forms-library'

@NgModule({
  declarations: [
    NewPostPage,
  ],
  imports: [
    IonicPageModule.forChild(NewPostPage), DynamicFormsModule
  ],
})
export class NewPostPageModule { }
