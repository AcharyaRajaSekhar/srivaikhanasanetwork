import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup } from '@angular/forms';
import { SvnMetaDataService } from '../../../platform/providers/svn-meta-data.service';
import { ToastMessageService, BackButtonEventHandler, BusyIndicatorService } from '@sri-vaikhanasa-network/ion-platform-services-library';
import { PostsProvider } from '../../../providers/posts/posts';
import { PhotoUploaderService } from '../../../providers/photo-uploader/photo-uploader.service';
import { UploadableImage } from '../../../models/uploadable-image-model';
import { ControlsService, ControlBase } from '@sri-vaikhanasa-network/ion-dynamic-forms-library';

@IonicPage()
@Component({
  selector: 'page-new-post',
  templateUrl: 'new-post.html',
})
export class NewPostPage {

  pageConfig: any;

  controls: ControlBase<any>[];
  form: FormGroup;
  submitted: any;
  formConfig: any = {};
  backButtonEventHandle: any;

  constructor(
    private formConfigService: SvnMetaDataService,
    private controlSvc: ControlsService,
    private backButtonHandler: BackButtonEventHandler,
    private navCtrl: NavController,
    private busyIndicator: BusyIndicatorService,
    private toastr: ToastMessageService,
    private postsSvc: PostsProvider,
    private uploadSvc: PhotoUploaderService,
    private navParams: NavParams) {
    this.pageConfig = this.navParams.data;
    this.form = new FormGroup({});
  }

  ionViewWillEnter() {

    this.backButtonEventHandle = this.backButtonHandler.registerPageLevelEvent();

    this.formConfigService.getFormConfigByFormName(this.pageConfig.formId).then(data => {
      if (data) {
        this.formConfig = data;
        this.controls = this.controlSvc.getControls(data.controls);
        this.form.valueChanges
          .subscribe(val => {
            this.submitted = val;
          });
      } else {
        this.toastr.show("Form config missing", true);
        this.goBack();
      }
    });

  }

  ionViewWillLeave() {
    this.backButtonHandler.unregisterPageLevelEvent(this.backButtonEventHandle);
  }

  submitForm($event) {
    if (this.form.valid) {
      this.busyIndicator.show();
      let data = this.submitted;
      data.type = this.pageConfig.id;
      if (data['photos'] && data['photos'].length > 0) {
        this.toastr.show("Uploading images and data");
        this.uploadPhotosIfAny(data['photos']).then(urls => {          
          data['photos'] = urls;
          this.postsSvc.addPostByType(data).then(() => {
            console.log('done');
            this.busyIndicator.hide();
            this.goBack();
          })
        })
      }
      else {
        this.toastr.show("Uploading data only");
        this.postsSvc.addPostByType(data).then(() => {
          console.log('done');
          this.busyIndicator.hide();
          this.goBack();
        })
      }
      console.log("Success\n", this.submitted);
    }
    else {
      console.log("Form is incomplete\n", this.submitted);
    }

  }

  uploadPhotosIfAny(photos: Array<string>): Promise<any> {
    return photos.reduce((photosChain: any, currentPhoto: any) => {
      return photosChain.then((uploadedUrls) => {
        let temp = new UploadableImage(currentPhoto);
        return this.uploadSvc.upload(temp)
          .then(currentUrl => [...uploadedUrls, currentUrl], err => this.toastr.show("Error while uploading photo: " + JSON.stringify(err), true));
      });
    }, Promise.resolve([]));
  }

  goBack() {
    this.navCtrl.pop();
  }

}
