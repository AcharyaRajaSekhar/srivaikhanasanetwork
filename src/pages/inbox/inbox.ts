import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { BackButtonEventHandler } from '@sri-vaikhanasa-network/ion-platform-services-library';

@IonicPage()
@Component({
  selector: 'page-inbox',
  templateUrl: 'inbox.html',
})
export class InboxPage {

  constructor(public navCtrl: NavController, private backButtonHandler: BackButtonEventHandler) {
  }

  goBack() {
    this.backButtonHandler.goBackToHome();
  }

}
