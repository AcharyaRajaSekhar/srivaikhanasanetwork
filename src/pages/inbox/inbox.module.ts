import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InboxPage } from './inbox';
import { SuperTabsModule } from 'ionic2-super-tabs';

@NgModule({
  declarations: [
    InboxPage,
  ],
  imports: [
    IonicPageModule.forChild(InboxPage),
    SuperTabsModule,
  ],
})
export class InboxPageModule {}
