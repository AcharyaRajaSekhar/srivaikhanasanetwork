import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth';
import { UploadIndicatorService, SentryLoggerService } from '@sri-vaikhanasa-network/ion-platform-services-library';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  slides: Array<string> = [
    "assets/imgs/slides/1.jpg"
    // "assets/imgs/slides/2.jpg",
    // "assets/imgs/slides/3.jpg",
    // "assets/imgs/slides/4.jpg"
  ];


  constructor(private auth: AuthService, 
    private log: SentryLoggerService,
    private uploadInd: UploadIndicatorService, private events: Events) {
    
  }

  ionViewCanEnter(): Promise<boolean> {
    return this.auth.isLoggedIn();
  }

  logout() {
    this.auth.signOut();
  }

  temple: any = {
    id: "5gKcKuolF7nL27yyHtN4",
    type: "temples"
  }

  network() {
    
  }

  progress: number = 0;
  handle: any;
  upload() {
    this.progress = 0;
    this.uploadInd.show();
    this.handle = setInterval(() => { this.ticks(); this.progress += 10; }, 1000);
  }

  ticks() {
    console.log(this.progress);
    this.events.publish('on-upload-status-update', {
      current: 1,
      total: 1,
      progress: this.progress
    })
    if (this.progress === 100) {
      this.uploadInd.hide();
      clearInterval(this.handle);
    }
  }

}
