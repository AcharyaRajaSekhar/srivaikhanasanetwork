import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditProfilePage } from './edit-profile';
import { ComponentsModule } from '../../../components/components.module';
import { CustomFormControlsModule } from '@sri-vaikhanasa-network/ion-dynamic-forms-library';

@NgModule({
  declarations: [
    EditProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(EditProfilePage),
    ComponentsModule,
    CustomFormControlsModule
  ],
})
export class EditProfilePageModule {}
