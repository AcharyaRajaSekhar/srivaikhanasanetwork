import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { UserProfileService } from '../../../providers/user-profile/user-profile';
import { UserProfileModel } from '../../../models/user-profile-model';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { BackButtonEventHandler } from '@sri-vaikhanasa-network/ion-platform-services-library';

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  userProfile: UserProfileModel;
  backButtonEventHandle: any;

  constructor(
    private navCtrl: NavController,
    private profile: UserProfileService,
    private backButtonHandler: BackButtonEventHandler) { }

  ngOnInit() {
    this.profile.getMyProfile()
      .then(p => {
        this.userProfile = p;
      });
  }

  saveProfile() {
    this.profile.setMyProfile(this.userProfile).then(() => this.goBack()).catch(() => { });
  }

  goBack() {
    this.navCtrl.pop();
  }

  ionViewWillEnter() {
    this.backButtonEventHandle = this.backButtonHandler.registerPageLevelEvent();
  }

  ionViewWillLeave() {
    this.backButtonHandler.unregisterPageLevelEvent(this.backButtonEventHandle);
  }
}
