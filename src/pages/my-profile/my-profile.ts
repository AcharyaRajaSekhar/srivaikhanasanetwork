import { Component } from '@angular/core';
import { IonicPage, NavController, ActionSheetController } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth';
import { UserProfileService } from '../../providers/user-profile/user-profile';
import { UserProfileModel } from '../../models/user-profile-model';
import { AddressApiProvider } from '../../providers/address-api/address-api';
import { ToastMessageService, BackButtonEventHandler, CallNumberService, PhotoViewerService, ImagePickerService, CameraService, UploadIndicatorService } from '@sri-vaikhanasa-network/ion-platform-services-library';

@IonicPage()
@Component({
  selector: 'page-my-profile',
  templateUrl: 'my-profile.html',
})
export class MyProfilePage {

  user: any;
  userProfile: UserProfileModel = { address: {} }

  constructor(
    public navCtrl: NavController,
    private backButtonHandler: BackButtonEventHandler,
    private profile: UserProfileService,
    private dialer: CallNumberService,
    private addressApi: AddressApiProvider,
    private photoViewer: PhotoViewerService,
    private uploadIndicator: UploadIndicatorService,
    private actionSheetCtrl: ActionSheetController,
    private toastr: ToastMessageService,
    private imagePickerSvc: ImagePickerService,
    private cameraSvc: CameraService,
    private auth: AuthService) {
    this.user = this.auth.user;
  }

  ionViewCanEnter(): Promise<boolean> {
    return this.auth.isLoggedIn();
  }

  ionViewDidEnter() {
    this.profile.getMyProfile()
      .then(p => {
        this.userProfile = p;
      });
  }

  changePicture() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Choose photo from',
      buttons: [
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            this.cameraSvc.pickPhoto(1).then(url => {
              if (url) {
                this.userProfile.photoURL = url;
                this.upload(url);
              }
            }).catch(err => this.toastr.show(err, true));
          }
        }, {
          text: 'Gallery',
          icon: 'svnicon-gallery1',
          handler: () => {
            this.imagePickerSvc.pickOne().then(url => {
              if (url) {
                this.userProfile.photoURL = url;
                this.upload(url);
              } else {
                this.toastr.show("Something wrong with image pickr service: " + JSON.stringify(url), true);
              }
            }).catch(err => this.toastr.show(err, true));
          }
        }
      ]
    });
    actionSheet.present();
  }

  upload(url) {
    this.uploadIndicator.show();
    this.profile.setMyProfilePic(url).then(() => {
      this.toastr.show("Profile pic updated...");
      this.uploadIndicator.hide();
    }).catch(err => {
      this.toastr.show("Error while updating profile pic : " + JSON.stringify(err), true);
      this.uploadIndicator.hide();
    });
  }

  getPostOfficeDetails() {
    return this.addressApi.getPostOfficeDetails(this.userProfile.address);
  }

  signOut() {
    this.auth.signOut();
  }

  callMobile() {
    if (this.userProfile && this.userProfile.phoneNumber) {
      this.dialer.call(this.userProfile.phoneNumber);
    }
  }

  editProfile() {
    this.navCtrl.push("EditProfilePage");
  }

  zoom(imgURL) {
    this.photoViewer.show(imgURL, 'My Profile Photo');
  }

  goBack() {
    this.backButtonHandler.goBackToHome();
  }
}
