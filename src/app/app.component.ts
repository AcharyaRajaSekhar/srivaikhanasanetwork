import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HeaderColor } from '@ionic-native/header-color';

import { AuthService } from '../providers/auth/auth';
import { CacheService } from 'ionic-cache';
import { ToastMessageService, BackButtonEventHandler } from '@sri-vaikhanasa-network/ion-platform-services-library';

import { NetworkAlertService } from '@sri-vaikhanasa-network/ngx-network-alert';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any;
  isAuthCheckCompleted = false;
  showSplash: boolean = true;
  backButtonHandle: any;

  @ViewChild('appnav') navCtrl: NavController

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private headerColor: HeaderColor,
    private backButtonHandler: BackButtonEventHandler,
    private cache: CacheService,
    private network: NetworkAlertService,
    private toastr: ToastMessageService,
    private auth: AuthService) {
    this.platform.ready()
      .then(() => {
        if (this.platform.is('cordova')) {
          this.statusBar.styleDefault();
          this.statusBar.overlaysWebView(false);
          this.statusBar.backgroundColorByHexString('#5928B1');
          this.headerColor.tint('#5928B1');
          this.splashScreen.hide();
        }
        this.CheckAuth();
        this.cache.setDefaultTTL(30 * 24 * 60 * 60); // set default cache TTL for 30 days
        this.cache.setOfflineInvalidate(false); // cache is valid even after the app goes offline
        this.backButtonHandler.registerAppLevelHandler();
        this.network.initialize({ "icon": "svnicon-cloud-off", "message": "You are offline" });
        this.network.monitor.subscribe(isOnline => {
          if (isOnline) {
            this.backButtonHandle = this.backButtonHandler.disableEvent();
          } else if (this.backButtonHandle) {
            this.backButtonHandle();
          }
        })
      });
  }

  CheckAuth(): Promise<any> {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.auth.user.subscribe(u => {
          this.rootPage = u ? "ContainerPage" : "LoginPage";
          this.isAuthCheckCompleted = u ? true : false;
          this.showSplash = false;
          resolve(true);
        }, err => {
          this.toastr.show("Error occured while initial authentication check: " + JSON.stringify(err), true);
          this.rootPage = "LoginPage";
          this.isAuthCheckCompleted = false;;
          this.showSplash = false;
          resolve(true);
        });
      }, 500);
    });
  }
}

