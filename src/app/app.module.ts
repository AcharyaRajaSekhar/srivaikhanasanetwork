import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HeaderColor } from '@ionic-native/header-color';
import { GooglePlus } from '@ionic-native/google-plus';
import { ImagePicker } from '@ionic-native/image-picker';
import { Crop } from '@ionic-native/crop';
import { CacheModule } from "ionic-cache";
import { Camera } from '@ionic-native/camera';
import { Network } from '@ionic-native/network';
import { File } from '@ionic-native/file';
import { Geolocation } from '@ionic-native/geolocation';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { CallNumber } from '@ionic-native/call-number';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

import { MyApp } from './app.component';

import { ComponentsModule } from '../components/components.module'
import { AuthService } from '../providers/auth/auth';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { UserProfileService } from '../providers/user-profile/user-profile';
import { AddressApiProvider } from '../providers/address-api/address-api';
import { SvnMetaDataService } from '../platform/providers/svn-meta-data.service';
import { PostsProvider } from '../providers/posts/posts';
import { TimestampService } from '../platform/providers/timestamp.service';
import { PipesModule } from '../pipes/pipes.module';
import { DatePipe } from '@angular/common';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { MySettingsRepo } from '../providers/my-settings/my-settings.service';
import { FIREBASE_CONFIG } from './firebase.config';
import { PhotoUploaderService } from '../providers/photo-uploader/photo-uploader.service';
import { CalendarEventsService } from '../providers/calendar-events/calendar-events';
import { GalleryProvider } from '../providers/gallery/gallery';

import { SvnPlatformServicesModule, SentryLoggerService, SentryErrorHandler, IonBusyIndicatorComponent, IonUploadIndicatorComponent } from '@sri-vaikhanasa-network/ion-platform-services-library';

import { NetworkAlertModule  } from "@sri-vaikhanasa-network/ngx-network-alert";;

import * as Sentry from 'raven-js';
Sentry.config('https://baf78b0ae79443e294a5266e6446f8e8@sentry.io/1305044').install();

console.log("Sentry setup: " + Sentry.isSetup());

Sentry.captureMessage("Tring tring");

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      scrollAssist: true,
      autoFocusAssist: false,
      preloadModules: true,
      scrollPadding: false,
      tabsHideOnSubPages: true
    }),
    HttpClientModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFirestoreModule,
    ComponentsModule,
    SuperTabsModule.forRoot(),
    PipesModule,
    CacheModule.forRoot({ keyPrefix: 'svn-app-cache-' }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    SvnPlatformServicesModule,
    NetworkAlertModule
  ],
  bootstrap: [IonicApp, IonBusyIndicatorComponent, IonUploadIndicatorComponent],
  entryComponents: [
    MyApp,
  ],
  providers: [
    GooglePlus,
    StatusBar,
    SplashScreen,
    HttpClient,
    { provide: ErrorHandler, useClass: SentryErrorHandler },
    SentryLoggerService,
    AuthService,
    UserProfileService,
    AddressApiProvider,
    HeaderColor,
    SvnMetaDataService,
    PostsProvider,
    TimestampService,
    DatePipe,
    PhotoViewer,
    ImagePicker,
    Crop,
    Camera,
    MySettingsRepo,
    Network,
    PhotoUploaderService,
    File,
    Geolocation,
    CallNumber,
    LaunchNavigator,
    CalendarEventsService,
    GalleryProvider,
  ]
})
export class AppModule { }
