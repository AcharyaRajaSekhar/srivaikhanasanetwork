cd "%~dp0"
echo // Auto generated style doc > svn.icons.scss
echo ion-icon { >> svn.icons.scss
echo ^&[class*=svnicon-]{mask-size:contain;mask-position:50% 50%;mask-repeat:no-repeat;background:currentColor;width:1em;height:1em} >> svn.icons.scss
FOR %%I in (../../assets/imgs/icons/*.svg) DO echo ^&[class*=svnicon-%%~nI]{mask-image:url(../assets/imgs/icons/%%I)} >> svn.icons.scss
echo } >> svn.icons.scss
pause