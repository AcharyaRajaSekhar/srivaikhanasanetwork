export class UserProfileModel {
  id?: string;
  photoURL?: string;
  name?: string;
  email?: string;
  phoneNumber?: string;
  gender?: string;
  dob?: Date;
  gothram?: string;
  surname?: string;
  address?: any;
}