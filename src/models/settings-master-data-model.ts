class Language {
    order?: number;
    code?: string;
    title?: string;
}

export class SettingsMasterData {
    languages?: Array<Language>;
}