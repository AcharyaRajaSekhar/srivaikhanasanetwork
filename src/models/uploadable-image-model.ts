export class UploadableImage {

  fileContent: string;
  downloadUrl: string;
  progress: number;

  constructor(fileContent: string) {
    this.fileContent = fileContent;
  }
}