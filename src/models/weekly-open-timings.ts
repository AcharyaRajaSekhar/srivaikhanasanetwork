import { Time } from "@angular/common";

export class WeeklyOpenTimings {
    Sunday: Array<{ start: Time, end: Time }> = [];
    Monday: Array<{ start: Time, end: Time }> = [];
    Tuesday: Array<{ start: Time, end: Time }> = [];
    Wednesday: Array<{ start: Time, end: Time }> = [];
    Thursday: Array<{ start: Time, end: Time }> = [];
    Friday: Array<{ start: Time, end: Time }> = [];
    Saturday: Array<{ start: Time, end: Time }> = [];
}

