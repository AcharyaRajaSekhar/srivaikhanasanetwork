import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from '../auth/auth';
import { TimestampService } from '../../platform/providers/timestamp.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CalendarEventsService {

  constructor(private db: AngularFirestore, private auth: AuthService, private utility: TimestampService) { }

  getEventsFromGivenDate(type, id, date): Observable<Array<any>> {
    return this.db.collection('calendarevents',
      ref => ref.where('parentType', '==', type).where('parentId', '==', id).where('endDate', '>', date))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({
          id: c.payload.doc.id,
          ...c.payload.doc.data()
        }))
      })
  }

  addEvent(event) {
    return new Promise((resolve, reject) => {
      this.auth.user.subscribe(u => {
        event.createdBy = this.db.doc('users/' + u.uid).ref;
        event.isActive = true;
        event.isVerified = false;
        event.createdAt = this.utility.timestamp;
        event.updatedAt = this.utility.timestamp;
        this.db.collection('calendarevents').add(event).then(res => {
          resolve();
        })
      })
    });
  }

}
