import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { UserProfileModel } from '../../models/user-profile-model';
import { AuthService } from '../auth/auth';
import { ToastMessageService, BusyIndicatorService } from '@sri-vaikhanasa-network/ion-platform-services-library';
import { PhotoUploaderService } from '../photo-uploader/photo-uploader.service';
import { GalleryProvider } from '../gallery/gallery';

@Injectable()
export class UserProfileService {

  UserProfilesCollection: AngularFirestoreCollection<UserProfileModel>; //Firestore collection
  UserProfiles: Observable<UserProfileModel[]>; // read collection

  constructor(private db: AngularFirestore,
    private auth: AuthService,
    private busyIndicator: BusyIndicatorService,
    private gallery: GalleryProvider,
    private uploadSvc: PhotoUploaderService,
    private toast: ToastMessageService) {
    this.UserProfilesCollection = this.db.collection('users'); //ref()
    this.UserProfiles = this.UserProfilesCollection.valueChanges()
  }

  getMyProfile(): Promise<UserProfileModel> {
    return new Promise((resolve, reject) => {
      this.auth.user.subscribe(u => {
        if (u) {
          this.UserProfilesCollection.doc(u.uid)
            .valueChanges().subscribe((up: UserProfileModel) => {
              if (!up) { up = { address: {} } }
              up.id = u.uid;
              up.phoneNumber = u.phoneNumber;
              resolve(up);
            })
        }
      })
    });
  }

  setMyProfile(data: UserProfileModel) {
    return new Promise((resolve, reject) => {
      this.busyIndicator.show()
      this.auth.user.subscribe(u => {
        this.UserProfilesCollection.doc(u.uid).update(data)
          .then(() => {
            this.busyIndicator.hide()
            this.toast.show("Profile updated...");
            resolve();
          }).catch((e) => {
            this.busyIndicator.hide()
            this.toast.show("Error: " + JSON.stringify(e), true);
            reject();
          });
      });
    });
  }

  setMyProfilePic(picURL) {
    return new Promise((resolve, reject) => {
      if (picURL) {
        this.uploadSvc.uploadSingle(picURL).then(url => {
          if (url && this.auth.userUID) {
            let temp = {
              photos: [url],
              parentId: this.auth.userUID,
              parentType: "user-profile-photo",
              dttm: new Date()
            }
            this.gallery.setOnlyOne(temp).then(() => {
              resolve();
            }).catch(err => reject(err));
          }
        }).catch(err => reject(err));
      } else {
        reject("Invalid photo URL");
      }
    });
  }
}