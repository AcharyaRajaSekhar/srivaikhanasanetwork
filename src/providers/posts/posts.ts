import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../auth/auth';
import { TimestampService } from '../../platform/providers/timestamp.service';

@Injectable()
export class PostsProvider {

  constructor(private db: AngularFirestore, private auth: AuthService, private utility: TimestampService) { }

  getPostsByType(type): Observable<Array<any>> {
    return this.db.collection('posts', ref => ref.where('type', '==', type)).snapshotChanges()
      .map(changes => {
        return changes.map(c => ({
          id: c.payload.doc.id,
          ...c.payload.doc.data()
        }))
      })
  }

  addPostByType(data) {
    return new Promise((resolve, reject) => {
      this.auth.user.subscribe(u => {
        data.createdby = this.db.doc('users/' + u.uid).ref;
        data.isActive = true;
        data.isVerified = false;
        data.createdAt = this.utility.timestamp;
        data.updatedAt = this.utility.timestamp;
        this.db.collection('posts').add(data).then(res => {
          resolve();
        })
      })
    });
  }

  patchUpdatePostById(id, field, value): Promise<any> {
    let docRef = this.db.collection('posts').doc(id);
    let data = {};
    data[field] = value;
    return docRef.update(data);
  }
}
