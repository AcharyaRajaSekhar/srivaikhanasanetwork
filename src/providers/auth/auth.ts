import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { first } from 'rxjs/operators/first';
import { GooglePlus } from '@ionic-native/google-plus';
import { Platform, App } from 'ionic-angular';
import { ToastMessageService, BusyIndicatorService } from '@sri-vaikhanasa-network/ion-platform-services-library';

import * as Sentry from 'raven-js';

@Injectable()
export class AuthService {

  userUID: string;
  user: Observable<firebase.User>;
  busyInd: any;

  constructor(
    private app: App,
    private afAuth: AngularFireAuth,
    private gplus: GooglePlus,
    private platform: Platform,
    private busyIndicator: BusyIndicatorService,
    private toast: ToastMessageService) {
    this.user = this.afAuth.authState;
    this.user.subscribe(u => {
      if (u) {
        this.userUID = u.uid;
        Sentry.setUserContext(u);
        Sentry.captureMessage("I got updated");
      }
    })
  }

  isLoggedIn(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.afAuth.authState.pipe(first()).toPromise().then(u => {
        if (u) resolve(true);
        else resolve(false);
      }).catch(e => resolve(false))
    });
  }

  googleLogin() {
    this.busyIndicator.show();
    if (this.platform.is('cordova')) {
      this.nativeGoogleLogin();
    } else {
      this.webGoogleLogin();
    }
  }

  async nativeGoogleLogin(): Promise<any> {
    try {

      const gplusUser = await this.gplus.login({
        'webClientId': '1013783687233-g70qk7qph2gj4kbo2hoib5vl4j0kcfes.apps.googleusercontent.com',
        'offline': true,
        'scopes': 'profile email'
      })
      this.app.getRootNav().setRoot('ContainerPage');
      this.busyIndicator.hide();
      return await this.afAuth.auth.signInWithCredential(
        firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken)
      );

    } catch (err) {
      this.busyIndicator.hide();
      this.toast.show(JSON.stringify(err), true);
    }
  }

  async webGoogleLogin(): Promise<void> {
    try {

      const provider = new firebase.auth.GoogleAuthProvider();
      const credential = await this.afAuth.auth.signInWithPopup(provider);
      console.log(credential);
      this.app.getRootNav().setRoot('ContainerPage');
      this.busyIndicator.hide();

    } catch (err) {
      this.busyIndicator.hide();
      this.toast.show(JSON.stringify(err), true);
    }
  }

  signOut() {
    this.app.getRootNav().setRoot('LoginPage');
    if (this.platform.is('cordova')) {
      this.gplus.logout();
    }
    this.afAuth.auth.signOut().then(() => {
      // window.location.reload();
      this.toast.show("Logged out successfully");
    })
  }

}
