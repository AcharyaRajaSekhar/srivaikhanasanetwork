import { Injectable } from '@angular/core';
import { UploadableImage } from '../../models/uploadable-image-model';
import firebase from 'firebase';
import { ToastMessageService } from '@sri-vaikhanasa-network/ion-platform-services-library';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from '../auth/auth';
import { TimestampService } from '../../platform/providers/timestamp.service';
import { Events } from 'ionic-angular';

@Injectable()
export class PhotoUploaderService {

  constructor(private toastr: ToastMessageService,
    private db: AngularFirestore,
    private auth: AuthService,
    private events: Events,
    private utility: TimestampService) { }

  uploadSingle(photo, current = 1, total = 1) {
    return new Promise((resolve, reject) => {
      let id = this.db.createId();
      let storageRef = firebase.storage().ref('photos/' + id);
      let uploadTask = storageRef.putString(photo, 'data_url');
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: any) => {
          let progress = ((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
          this.events.publish('on-upload-status-update', {
            current: current,
            total: total,
            progress: Math.trunc(((Math.trunc(progress) + ((current - 1) * 100)) / total))
          })
        },
        (error) => {
          this.toastr.show("Error occured while uploading: " + JSON.stringify(error), true);
          reject(error);
        },
        () => {
          // upload success
          uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
            resolve(downloadURL);
          });
        })
    });
  }

  private uploadChain(photos): Promise<any> {
    return photos.reduce((previous: any, current: string, index: number, arr: any) => {
      return previous.then(accumulator => {
        return this.uploadSingle(current, index + 1, arr.length)
          .then(currentUrl => [...accumulator, currentUrl]).catch(err => console.log(err));
      });
    }, Promise.resolve([]));
  }

  uploadMultiple(photos: Array<string>): Promise<any> {
    return new Promise((resolve, reject) => {
      if (photos && photos.length > 0) {
        this.uploadChain(photos).then(urls => resolve(urls)).catch(err => reject(err));
      }
      else {
        reject("Empty list of photos not allowed to upload...");
      }
    });
  }

  upload(payload: UploadableImage): Promise<any> {
    return new Promise((resolve) => {
      this.auth.user.subscribe(u => {
        var temp = {
          createdBy: this.db.doc('users/' + u.uid).ref,
          createdAt: this.utility.timestamp,
          isActive: true,
          folder: 'photos'
        };

        this.db.collection('uploads').add(temp).then(docRef => {
          let storageRef = firebase.storage().ref('photos/' + docRef.id);
          let uploadTask = storageRef.putString(payload.fileContent, 'data_url');

          uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot: any) => {
              // upload in progress          
              payload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
              console.log(payload.progress);
            },
            (error) => {
              this.toastr.show("Error occured while uploading: " + JSON.stringify(error), true);
            },
            () => {
              // upload success
              uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
                console.log(downloadURL);
                payload.downloadUrl = downloadURL
                resolve(payload.downloadUrl);
              });
            });
        });
      });
    });
  }

}
