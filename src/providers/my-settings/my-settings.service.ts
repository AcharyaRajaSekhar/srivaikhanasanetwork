import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SvnMetaDataService } from '../../platform/providers/svn-meta-data.service';
import { SettingsMasterData } from '../../models/settings-master-data-model';
import { MySettingsData } from '../../models/my-settings-model';

@Injectable()
export class MySettingsRepo {

  masterData: SettingsMasterData = {};
  selection: MySettingsData = {};

  constructor(
    private metaDataSvc: SvnMetaDataService,
    private translate: TranslateService) {
    this.loadMasterData();
    this.loadMyData();
  }

  loadMasterData(): any {
    this.metaDataSvc.getLanguagesConfig().then(data => {
      this.masterData.languages = data;
    })
  }

  loadMyData() {
    this.selection.language = this.selection.language || 'en';
    this.translate.setDefaultLang('en');
    this.translate.use(this.selection.language);
  }

  setMyLanguage() {
    console.log(this.selection.language);
    this.translate.use(this.selection.language || 'en');
  }

}
