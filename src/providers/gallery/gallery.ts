import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from '../auth/auth';
import { TimestampService } from '../../platform/providers/timestamp.service';
import { Observable } from 'rxjs/Observable';
import { first } from 'rxjs/operators/first';

@Injectable()
export class GalleryProvider {


  constructor(private db: AngularFirestore,
    private auth: AuthService,
    private utility: TimestampService) {

  }

  get(type, id): Observable<Array<any>> {
    return this.db.collection('gallery',
      ref => ref.where('parentType', '==', type).where('parentId', '==', id))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({
          id: c.payload.doc.id,
          ...c.payload.doc.data()
        }))
      })
  }

  private ifExists(id, type): Observable<any> {
    return this.db.collection('gallery',
      ref => ref.where('parentId', '==', id).where('parentType', '==', type))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({
          id: c.payload.doc.id,
          ...c.payload.doc.data()
        }))
      }).pipe(first());
  }

  setOnlyOne(data): Promise<any> {
    return new Promise((resolve) => {
      this.ifExists(data.parentId, data.parentType).subscribe(docs => {
        if (docs && docs.length === 1) {
          let doc = docs[0];
          this.db.collection('gallery').doc(doc.id).delete().then(() => {
            let temp = {
              ...data,
              createdBy: this.auth.userUID ? this.db.doc('users/' + this.auth.userUID).ref : undefined,
              createdAt: this.utility.timestamp,
              isActive: true,
              isFromFlickr: false
            };
            this.db.collection('gallery').add(temp).then(() => {
              resolve();
            })
          })
        } else {
          let temp = {
            ...data,
            createdBy: this.auth.userUID ? this.db.doc('users/' + this.auth.userUID).ref : undefined,
            createdAt: this.utility.timestamp,
            isActive: true,
            isFromFlickr: false
          };
          this.db.collection('gallery').add(temp).then(() => {
            resolve();
          })
        }
      })
    });
  }

  update(data): Promise<any> {
    return new Promise((resolve) => {
      this.auth.user.subscribe(u => {
        var temp = {
          ...data,
          createdBy: this.db.doc('users/' + u.uid).ref,
          createdAt: this.utility.timestamp,
          isActive: true,
          isFromFlickr: false
        };
        this.db.collection('gallery').add(temp).then(docRef => {
          console.log(docRef.id);
          resolve();
        })
      })
    });
  }

  removePics(docRef: any, photos: Array<string>): Promise<any> {
    if (photos.length > 0) {
      let data = {};
      data['photos'] = photos;
      return this.db.collection('gallery').doc(docRef).update(data);
    } else {
      return this.db.collection('gallery').doc(docRef).delete();
    }
  }
}
