import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { first } from 'rxjs/operators/first';
import { CacheService } from "ionic-cache";
import { BusyIndicatorService } from '@sri-vaikhanasa-network/ion-platform-services-library';

@Injectable()
export class AddressApiProvider {

  constructor(public http: HttpClient, private busyIndicator: BusyIndicatorService, private cache: CacheService) { }

  getDetailsByPINCode(pinCode: number): Promise<any> {
    let url = 'https://indian-pincodes.firebaseio.com/records.json?orderBy=%22pincode%22&equalTo=%22' + pinCode + '%22&print=pretty';
    let cacheKey = url;
    let groupKey = "postaldata"
    let request = this.http.get(url);
    let delayType = 'none';
    let ttl = 30 * 24 * 60 * 60;
    var postOffices = [];
    return new Promise((resolve, reject) => {
      this.busyIndicator.show()
      let response = this.cache.loadFromDelayedObservable(cacheKey, request, groupKey, ttl, delayType);
      response.pipe(first()).toPromise().then(data => {
        if (data) {
          for (var x in data) {
            if (data[x].deliverystatus == "Delivery") {
              postOffices.push(data[x]);
            }
          }
          this.busyIndicator.hide();
          resolve(postOffices);
        }
      }, err => {
        this.busyIndicator.hide();
        reject(err);
      })
    })
  }

  getPostalDetailsForPlainDisplay(postal) {
    let details = '';
    if (postal) {
      if (postal.officename) {
        details = postal.officename.replace('S.O', ' (Post), ').replace('B.O', ' (Post), ').replace('H.O', ' (Post), ');
        details += postal.taluk + ' (Taluk), ';
        details += postal.districtname + ' (District), ';
        details += postal.statename + ', PIN:' + postal.pincode + ", India";
      }
    } else {
      details = '';
    }
    return details;
  }

  getSimpleAddress(postal) {
    let details = '';
    if (postal) {
      if (postal.officename) {
        details = postal.officename.replace(' S.O', ', ').replace(' B.O', ', ').replace(' H.O', ', ');
        details += postal.districtname + ', ';
        details += postal.statename;
      }
    }
    return details;
  }

  getPostOfficeDetails(address) {
    let details = '';
    let postal: any;
    if (address) {
      details = address.line ? address.line + ", " : '';
      postal = address;
    }
    if (postal) {
      if (postal.officename) {
        details += postal.officename.replace('S.O', ' (Post), ').replace('B.O', ' (Post), ').replace('H.O', ' (Post), ');
        details += postal.taluk + ' (Taluk), ';
        details += postal.districtname + ' (District), ';
        details += postal.statename + ', PIN:' + postal.pincode + ", India";
      }
    } else {
      details = '';
    }
    return details;
  }

  getMapsAddressDetails(address) {
    let details = '';
    let postal: any;
    if (address) {
      details = address.line + ", ";
      postal = address;
    }
    if (postal) {
      if (postal.officename) {
        details += postal.officename.replace('S.O', ', ').replace('B.O', ', ').replace('H.O', ', ');
        details += postal.taluk + ', ';
        details += postal.districtname + ', ';
        details += postal.statename + ', PIN:' + postal.pincode + ", India";
      }
    } else {
      details = '';
    }
    return details;
  }

}
