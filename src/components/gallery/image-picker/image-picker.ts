import { Component, NgZone } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { UploadableImage } from '../../../models/uploadable-image-model';
import { ToastMessageService, ImagePickerService, UploadIndicatorService } from '@sri-vaikhanasa-network/ion-platform-services-library';
import { PhotoUploaderService } from '../../../providers/photo-uploader/photo-uploader.service';
import { GalleryProvider } from '../../../providers/gallery/gallery';

@Component({
  selector: 'image-picker',
  templateUrl: 'image-picker.html'
})
export class ImagePickerComponent {

  parent: any = {};
  title: string;
  dttm: string = new Date().toISOString();
  photos: Array<string> = [];

  constructor(
    private viewCtrl: ViewController,
    private _ngZone: NgZone,
    private navParams: NavParams,
    private uploadIndicator: UploadIndicatorService,
    private toastr: ToastMessageService,
    private gallery: GalleryProvider,
    private uploadSvc: PhotoUploaderService,
    private imagePickerService: ImagePickerService) {
    this.parent = this.navParams.get('parent');
  }

  openImagePicker() {
    this.photos = [];
    this.imagePickerService.pick(10).then(photos => {
      this._ngZone.run(() => {
        this.photos = photos;
      })
    }, err => this.toastr.show(err, true));
  }

  clear() {
    this.photos = [];
  }

  upload() {
    if (!this.title) { this.toastr.show("Please enter album title", true); return; }
    if (!this.dttm) { this.toastr.show("Album date is mandatory", true); return; }

    this.uploadIndicator.show();
    this.uploadSvc.uploadMultiple(this.photos).then((urls) => {
      if (urls) {
        let temp = {
          photos: urls,
          parentId: this.parent.id,
          parentType: this.parent.type,
          title: this.title,
          dttm: new Date(this.dttm)
        }
        this.gallery.update(temp).then(() => {
          this.uploadIndicator.hide();
          this.toastr.show("Uploaded succesfully...");
          this.goBack();
        })
      }
    }).catch(err => { this.uploadIndicator.hide(); console.log(err); })
  }

  uploadPhotosIfAny(photos: Array<string>): Promise<any> {
    return photos.reduce((photosChain: any, currentPhoto: any) => {
      return photosChain.then((uploadedUrls) => {
        let temp = new UploadableImage(currentPhoto);
        return this.uploadSvc.upload(temp)
          .then(currentUrl => [...uploadedUrls, currentUrl], err => this.toastr.show("Error while uploading photo: " + JSON.stringify(err), true));
      });
    }, Promise.resolve([]));
  }

  goBack() {
    this.viewCtrl.dismiss();
  }

}
