import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { ImagePickerComponent } from './image-picker/image-picker';
import { GalleryProvider } from '../../providers/gallery/gallery';
import { PhotoViewerService, BusyIndicatorService } from '@sri-vaikhanasa-network/ion-platform-services-library';

@Component({
  selector: 'gallery',
  templateUrl: 'gallery.html'
})
export class GalleryComponent {

  @Input() parent: any = {};
  gallery: Array<any> = [];
  selection: Array<any> = [];

  constructor(
    private modalCtrl: ModalController,
    private gallerySvc: GalleryProvider,
    private photoViewer: PhotoViewerService,
    private busyIndicator: BusyIndicatorService,
  ) { }

  ngOnInit() {
    this.gallerySvc.get(this.parent.type, this.parent.id).subscribe((docs) => {
      this.gallery = docs;
    })
  }

  browse() {
    let picker = this.modalCtrl.create(ImagePickerComponent, { parent: { id: this.parent.id, type: this.parent.type } });
    picker.present();
  }

  zoom(imgURL, title, ai, pi) {
    if (this.selection.length === 0) {      
      this.photoViewer.show(imgURL, title);
    } else {
      this.onPress(ai, pi);
    }
  }

  onPress(ai, pi) {
    if (this.selection.filter(e => e.ai === ai && e.pi === pi).length === 0) {
      this.selection.push({ ai: ai, pi: pi });
    } else {
      this.selection = this.selection.filter(e => !(e.ai === ai && e.pi === pi));
    }
  }

  getOverlayClass(ai, pi) {
    let temp = "overlay";
    if (this.selection.filter(e => e.ai === ai && e.pi === pi).length > 0) {
      temp += " show-overlay";
    }
    return temp;
  }

  remove() {
    if (this.selection.length > 0) {
      this.busyIndicator.show();
      let temp = [];
      for (let s of this.selection) {
        let docRef = this.gallery[s.ai].id;
        let photoRef = this.gallery[s.ai].photos[s.pi];
        if (temp.filter(t => t.id === docRef).length === 0) {
          let t1 = { id: docRef };
          t1['photos'] = [photoRef];
          temp.push(t1);
        } else {
          let ti = temp.findIndex(i => i.id === docRef);
          temp[ti].photos.push(photoRef);
        }
      }
      for (let tmp = 0; tmp < temp.length; tmp++) {
        let ti = this.gallery.findIndex(i => { return i.id == temp[tmp].id; });
        let remainingPhotos = this.gallery[ti].photos.filter(p => temp[tmp].photos.indexOf(p) === -1);
        this.gallerySvc.removePics(temp[tmp].id, remainingPhotos).then(() => {
          if (tmp == (temp.length - 1)) {
            this.busyIndicator.hide();
            this.selection = [];
          }
        })
      }
    }
  }

  clear() {
    this.selection = [];
  }
}
