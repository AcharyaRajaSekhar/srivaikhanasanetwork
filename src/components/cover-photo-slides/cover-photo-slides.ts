import { Component, Input } from '@angular/core';
import { PhotoViewer } from '@ionic-native/photo-viewer';

@Component({
  selector: 'cover-photo-slides',
  templateUrl: 'cover-photo-slides.html'
})
export class CoverPhotoSlidesComponent {
  
  @Input() slides: any;

  constructor(private photoViewer: PhotoViewer) { }

  zoom(imgURL) {
    this.photoViewer.show(imgURL);
  }
}
