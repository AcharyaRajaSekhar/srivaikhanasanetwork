import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { CalendarEventsService } from '../../../providers/calendar-events/calendar-events';
import { ToastMessageService, BusyIndicatorService } from '@sri-vaikhanasa-network/ion-platform-services-library';

@Component({
  selector: 'add-calendar-event',
  templateUrl: 'add-calendar-event.html'
})
export class AddCalendarEventComponent {
  event: any = {};

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private toastr: ToastMessageService,
    private busyIndicator: BusyIndicatorService,
    private calendarEventsSvc: CalendarEventsService) {
    this.event = this.navParams.get('event');
  }

  goBack() {
    this.viewCtrl.dismiss();
  }

  save() {
    this.busyIndicator.show();
    this.event.startDate = new Date(this.event.startDate);
    this.event.endDate = new Date(this.event.endDate);

    this.calendarEventsSvc.addEvent(this.event).then(() => {
      this.busyIndicator.hide();
      this.toastr.show("Event added successfully...");
      this.goBack();
    }).catch(err => {
      this.busyIndicator.hide();
      this.toastr.show("Error while adding calendar event: " + JSON.stringify(err), true);
    });
  }

}
