import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { AddCalendarEventComponent } from './add-calendar-event/add-calendar-event';
import { CalendarEventsService } from '../../providers/calendar-events/calendar-events';

@Component({
  selector: 'calendar',
  templateUrl: 'calendar.html'
})
export class CalendarComponent {
  date: any;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[];
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  selectedDate: any;
  loadingEvents: boolean = false;

  @Input() parent: any = {};
  events: Array<any> = [];

  constructor(
    private modalCtrl: ModalController,
    private calendarEventSvc: CalendarEventsService) { }

  ngOnInit() {
    this.monthNames = ['January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'];
    this.date = new Date();
    this.selectDate(this.date.getDate());
    this.getDaysOfMonth();
  }

  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if (this.date.getMonth() === new Date().getMonth() && this.date.getFullYear() === new Date().getFullYear()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    let firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    let prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for (let i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }

    let thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    for (let i = 0; i < thisNumOfDays; i++) {
      this.daysInThisMonth.push(i + 1);
    }

    let lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
    // let nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0).getDate();
    for (let i = 0; i < (6 - lastDayThisMonth); i++) {
      this.daysInNextMonth.push(i + 1);
    }
    let totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
    if (totalDays < 36) {
      for (let i = (7 - lastDayThisMonth); i < ((7 - lastDayThisMonth) + 7); i++) {
        this.daysInNextMonth.push(i);
      }
    }
  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
    this.selectDate(1);
  }

  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.getDaysOfMonth();
    this.selectDate(1);
  }

  loadEventsFromSelectedDate() {
    if (this.parent && this.parent.type && this.parent.id) {
      this.calendarEventSvc.getEventsFromGivenDate(this.parent.type, this.parent.id, this.selectedDate).subscribe(e => {
        this.events = [];
        for (var i in e) {
          this.events.push({
            ...e[i],
            collapsed: true
          })
        }
        this.loadingEvents = false;
      })
    } else {
      this.loadingEvents = false;
    }
  }

  checkEvent(day) {
    var hasEvent = false;
    var thisDate1 = this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + day + " 00:00:00";
    var thisDate2 = this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + day + " 23:59:59";
    this.events.forEach(event => {
      if (((event.startDate >= thisDate1) && (event.startDate <= thisDate2)) || ((event.endDate >= thisDate1) && (event.endDate <= thisDate2))) {
        hasEvent = true;
      }
    });
    return hasEvent;
  }

  selectDate(day) {
    this.selectedDate = new Date(Date.UTC(this.date.getFullYear(), this.date.getMonth(), day));
    this.loadingEvents = true;
    if (this.parent && this.parent.type && this.parent.id) {
      this.loadEventsFromSelectedDate();
    } else {
      setTimeout(() => {
        this.loadEventsFromSelectedDate();
      }, 2000);
    }
  }

  expandEvent(event) {
    this.events.map((listEvent) => {
      if (event == listEvent) {
        listEvent.collapsed = !listEvent.collapsed;
      } else {
        listEvent.collapsed = true;
      }
      return listEvent;
    });
  }

  getDayClass(day) {
    let c = "";
    if (this.currentDate === day) {
      c = "currentDate";
    } else {
      c = "otherDate";
    }
    if (this.checkEvent(day)) {
      c += " event-bullet";
    }
    if (this.selectedDate.getDate() === day && this.selectedDate.getMonth() === this.date.getMonth()) {
      c += " selected-date";
    }
    return c;
  }

  addNewEvent() {
    let event = {
      startDate: this.selectedDate.toISOString(),
      endDate: this.selectedDate.toISOString(),
      parentId: this.parent.id,
      parentType: this.parent.type
    }
    let m = this.modalCtrl.create(AddCalendarEventComponent, { event: event });
    m.present();
  }

}
