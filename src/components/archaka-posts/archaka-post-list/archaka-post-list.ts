import { Component, Input } from '@angular/core';

@Component({
  selector: 'archaka-post-list',
  templateUrl: 'archaka-post-list.html'
})
export class ArchakaPostListComponent {
  @Input() posts: any;
}
