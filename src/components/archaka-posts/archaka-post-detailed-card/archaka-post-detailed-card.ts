import { Component, Input } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { AddressApiProvider } from '../../../providers/address-api/address-api';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { CallNumberService } from '@sri-vaikhanasa-network/ion-platform-services-library';

@Component({
  selector: 'archaka-post-detailed-card',
  templateUrl: 'archaka-post-detailed-card.html'
})
export class ArchakaPostDetailedCardComponent {

  @Input() post: any = {};

  get slides(): Array<string> {
    if (this.post && this.post.photos && this.post.photos.length > 0) {
      return this.post.photos;
    } else {
      return ['assets/imgs/defaults/1.png'];
    }
  }

  constructor(
    private viewCtrl: ViewController,
    private dialer: CallNumberService,
    private addressApi: AddressApiProvider,
    private photoViewer: PhotoViewer) { }

  getPostOfficeDetails() {
    return this.addressApi.getPostOfficeDetails(this.post.address);
  }

  zoom(imgURL) {
    this.photoViewer.show(imgURL, this.post.name);
  }

  call(number) {
    this.dialer.call(number);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
