import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { ArchakaPostDetailedCardComponent } from '../archaka-post-detailed-card/archaka-post-detailed-card';
import { AddressApiProvider } from '../../../providers/address-api/address-api';
import { SvnModal80by80 } from '../../platform/SvnModal80by80';
import { SvnModal100by60 } from '../../platform/SvnModal100by60';
import { GoogleMapsComponent } from '../../google-maps/google-maps';
import { CallNumberService } from '@sri-vaikhanasa-network/ion-platform-services-library';

@Component({
  selector: 'archaka-post-basic-card',
  templateUrl: 'archaka-post-basic-card.html'
})
export class ArchakaPostBasicCardComponent {

  @Input() post: any;

  constructor(
    private modalCtrl: ModalController,
    private dialer: CallNumberService,
    private addressApi: AddressApiProvider) { }

  showDetails(post) {
    let profileModal = this.modalCtrl.create(SvnModal80by80, { target: ArchakaPostDetailedCardComponent, name: "post", value: post });
    profileModal.present();
  }

  getSimpleAddress() {
    return this.addressApi.getSimpleAddress(this.post.address);
  }

  getPhoto() {
    if (this.post.photos && this.post.photos.length > 0) {
      return this.post.photos[0];
    }
    return "assets/imgs/defaults/1.png";
  }

  call(number) {
    this.dialer.call(number);
  }

  openMaps() {
    let m = this.modalCtrl.create(SvnModal100by60, { target: GoogleMapsComponent, data: { latitude: '16.019599', longitude: '80.828827', title: 'Repalle Railway Station', address: 'Repalle, Andhra Pradesh 522265' } });
    m.present();
  }
}
