import { Component, ViewChild, ViewContainerRef, ComponentFactoryResolver } from "@angular/core";
import { NavParams } from "ionic-angular/navigation/nav-params";
import { ViewController } from "ionic-angular";

@Component({
  selector: 'svn-modal-80-by-80',
  template: `
<ion-content class="main-view">
    <div class="overlay" (click)="dismiss()"></div>
    <ion-scroll class="modal_content" scrollY="true">
      <ng-container #target></ng-container>
    </ion-scroll>
</ion-content>
  `,
  styles: [`
  .main-view{
    background: transparent;
  }
  .overlay {
    position: fixed;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: 1;
    opacity: .3;
    background-color: #000;
  }
  .modal_content {
    position: absolute;
    top: calc(10% - (10%/2));
    left: 0;
    right: 0;
    width: 90%;
    height: 90%;
    padding: 10px;
    z-index: 100;
    margin: 0 auto;
    padding: 10px;
    color: #333;
    background: #fff;
    background: -moz-linear-gradient(top, #fff 0%, #fff 100%);
    background: -webkit-linear-gradient(top, #fff 0%, #fff 100%);
    background: linear-gradient(to bottom, #fff 0%, #fff 100%);
    border-radius: 5px;
    box-shadow: 0 2px 3px rgba(51, 51, 51, .35);
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    overflow: hidden;
  }  
  `]
})
export class SvnModal80by80 {

  @ViewChild('target', {read: ViewContainerRef})
  target: ViewContainerRef;

  private targetComponentType: any;
  private targetComponentDataPropertyName: any;
  private targetComponentDataPropertyValue: any;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private viewCtrl: ViewController,
    private navParams: NavParams) {
      this.targetComponentType = this.navParams.get('target');
      this.targetComponentDataPropertyName = this.navParams.get('name');
      this.targetComponentDataPropertyValue = this.navParams.get('value');
  }

  ngOnInit() {
    let targetComponent = this.componentFactoryResolver.resolveComponentFactory(this.targetComponentType);
    this.target.clear();
    let comp = this.target.createComponent(targetComponent);
    comp.instance[this.targetComponentDataPropertyName] = this.targetComponentDataPropertyValue;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}