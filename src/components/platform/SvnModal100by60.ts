import { Component, ViewChild, ViewContainerRef, ComponentFactoryResolver } from "@angular/core";
import { NavParams } from "ionic-angular/navigation/nav-params";
import { ViewController } from "ionic-angular";

@Component({
  selector: 'svn-modal-100-by-60',
  template: `
<ion-content class="main-view">
    <div class="overlay" (click)="dismiss()"></div>
    <ion-scroll class="modal_content" scrollY="true">
      <ng-container #target></ng-container>
    </ion-scroll>
</ion-content>
  `,
  styles: [`
  .main-view{
    background: transparent;
  }
  .overlay {
    position: fixed;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: 1;
    opacity: .3;
    background-color: #000;
  }
  .modal_content {
    position: absolute;
    top: 30%;
    left: 0;
    right: 0;
    width: 100%;
    height: 70%;
    padding: 10px;
    z-index: 100;
    margin: 0 auto;
    padding: 10px;
    color: #333;
    background: #fff;
    background: -moz-linear-gradient(top, #fff 0%, #fff 100%);
    background: -webkit-linear-gradient(top, #fff 0%, #fff 100%);
    background: linear-gradient(to bottom, #fff 0%, #fff 100%);
    box-shadow: 0 2px 3px rgba(51, 51, 51, .35);
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    overflow: hidden;
  }
  .scroll-zoom-wrapper {
    height: 100% !important;
  }
  `]
})
export class SvnModal100by60 {

  @ViewChild('target', { read: ViewContainerRef })
  target: ViewContainerRef;

  private targetComponentType: any;
  private targetComponentData: any;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private viewCtrl: ViewController,
    private navParams: NavParams) {
    this.targetComponentType = this.navParams.get('target');
    this.targetComponentData = this.navParams.get('data');
  }

  ngOnInit() {
    let targetComponent = this.componentFactoryResolver.resolveComponentFactory(this.targetComponentType);
    this.target.clear();
    let comp = this.target.createComponent(targetComponent);
    for (let k in this.targetComponentData) {
      comp.instance[k] = this.targetComponentData[k].toString();
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}