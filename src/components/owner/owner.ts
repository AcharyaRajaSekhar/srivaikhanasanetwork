import { Component, Input } from '@angular/core';

@Component({
  selector: 'owner',
  templateUrl: 'owner.html'
})
export class OwnerComponent {
  @Input() post: any = {};
  @Input() panel: string;

  isAllowed(o): boolean {
    if (this.panel) {
      return this.panel.split('').indexOf(o) != -1;
    }
    return false;
  }
}