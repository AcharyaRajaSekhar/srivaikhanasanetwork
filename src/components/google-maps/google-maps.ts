import { Component, ViewChild, ElementRef, Input } from '@angular/core';
import { ToastMessageService } from '@sri-vaikhanasa-network/ion-platform-services-library';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';

declare var google;
declare var InfoBox;

@Component({
  selector: 'google-maps',
  templateUrl: 'google-maps.html'
})
export class GoogleMapsComponent {

  @Input() latitude: number;
  @Input() longitude: number;
  @Input() title: string;
  @Input() address: string;

  @ViewChild('map') mapElement: ElementRef;
  private map: any;
  private marker: any;
  private latLong: any;

  private mapOptions = {
    center: this.latLong,
    zoom: 7,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: true,
    streetViewControl: false,
    rotateControl: true,
    fullscreenControl: true,
    fullscreenControlOptions: {
      position: google.maps.ControlPosition.LEFT_BOTTOM
    }
  };

  constructor(
    private toastr: ToastMessageService,
    private launchNavigator: LaunchNavigator) { }

  ngOnInit() {
    this.latLong = new google.maps.LatLng(this.latitude, this.longitude);
    this.mapOptions.center = new google.maps.LatLng(this.latitude, this.longitude);
    this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);

    var directionsControlDiv = document.createElement('div');
    this.DirectionsControl(directionsControlDiv);
    this.map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(directionsControlDiv);
    var centerTheMarkerControlDiv = document.createElement('div');
    this.CenterTheMarkerControl(centerTheMarkerControlDiv);
    this.map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerTheMarkerControlDiv);

    this.addMarker();
    this.moveMarker();
  }

  moveMarker() {
    if (this.latitude && this.longitude) {
      this.latLong = new google.maps.LatLng(this.latitude, this.longitude);
      this.map.panTo(new google.maps.LatLng(this.latitude, this.longitude))
      this.marker.setPosition(this.latLong);
      console.log('marker added');
    }
  }

  addMarker() {
    this.marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter(),
      // icon: "assets/imgs/icons/pin.svg"
    });
    this.addInfoBox();
  }

  addInfoBox() {
    var boxText = document.createElement("div");
    boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background: white; padding: 5px; color: black;";
    boxText.innerHTML = `
    <div id="content">
    <div id="siteNotice"></div>
    <h4 id="firstHeading" class="firstHeading">` + this.title + `</h4>
    <div id="bodyContent">
       <p>` + this.address + `</p>
    </div>
 </div>
    `;
    var myOptions = {
      content: boxText
      , disableAutoPan: false
      , maxWidth: 0
      , pixelOffset: new google.maps.Size(-100, -160)
      , zIndex: null
      , boxStyle: {
        // background: "url('assets/js/infobox/tipbox.gif') no-repeat",
        opacity: 0.9,
        width: "200px"
      }
      , closeBoxMargin: "10px 2px 2px 2px"
      , closeBoxURL: "https://www.google.com/intl/en_us/mapfiles/close.gif"
      , infoBoxClearance: new google.maps.Size(1, 1)
      , isHidden: false
      , pane: "floatPane"
      , enableEventPropagation: false
    };

    var ib = new InfoBox(myOptions);

    google.maps.event.addListener(this.marker, "click", (e) => {
      ib.open(this.map, this.marker);
    });

    ib.open(this.map, this.marker);
  }

  DirectionsControl(controlDiv) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '0px solid #fff';
    controlUI.style.borderRadius = '2px';
    controlUI.style.boxShadow = '0 1px 2px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.margin = "5px;";
    controlUI.style.marginBottom = '36px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to show route map';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.padding = '5px';
    controlText.innerHTML = '<div jstcache="153" class="section-hero-header-directions-icon" style="background-image:url(assets/imgs/icons/direction.svg);background-size:20px;width:30px;height:30px;background-repeat: no-repeat;background-position: center;"></div>';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', () => {
      // let source = this.myLatLong.lat() + ',' + this.myLatLong.lng();
      // let destination = this.latLong.lat() + ',' + this.latLong.lng();
      // window.open("http://maps.google.com/maps?saddr=" + source + "&daddr=" + destination + "&ll=")

      let options: LaunchNavigatorOptions = {
        // start: 'London, ON',
        // app: LaunchNavigator.APP.GOOGLE_MAPS
      };
      let destination = [this.latLong.lat(), this.latLong.lng()];
      this.launchNavigator.navigate(destination, options)
        .then(
          () => this.toastr.show('Launched navigator'),
          (err) => this.toastr.show('Error launching navigator: ' + JSON.stringify(err), true)
        );
    });

  }

  CenterTheMarkerControl(controlDiv) {

    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '0px solid #fff';
    controlUI.style.borderRadius = '2px';
    controlUI.style.boxShadow = '0 1px 2px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.margin = "5px;";
    controlUI.style.marginBottom = '36px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to center the marker in map';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.padding = '5px';
    controlText.innerHTML = '<div jstcache="153" class="section-hero-header-directions-icon" style="background-image:url(assets/imgs/icons/placeholder.svg);background-size:20px;width:30px;height:30px;background-repeat: no-repeat;background-position: center;"></div>';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', () => {
      this.moveMarker();
    });

  }

}
