import { Component, ViewChild, Input, ElementRef, Renderer } from '@angular/core';

@Component({
  selector: 'accordion',
  templateUrl: 'accordion.html'
})
export class AccordionComponent {

  @ViewChild('accordianWrapper', { read: ElementRef }) expandWrapper;
  @Input('collapsed') collapsed;
  expandHeight: number = 100;

  constructor(public renderer: Renderer) { }

  ngAfterViewInit() {
    this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'height', this.expandHeight + 'px');
  }

}
