import { Component, Input } from '@angular/core';
import moment from 'moment';

@Component({
  selector: 'open-timings',
  templateUrl: 'open-timings.html'
})
export class OpenTimingsComponent {
  @Input() timings: any;
  @Input() title: string = "Timings";
  showFull: boolean = false;
  weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  weekdaysFull = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  toggle() {
    this.showFull = !this.showFull;
  }

  get fulltimings() {
    if (this.timings) {
      let temp = [];
      let i = 0;
      for (let w of this.weekdays) {
        let m1 = this.getAmPm(this.timings[w]['morning']['open']) + ' - ' + this.getAmPm(this.timings[w]['morning']['close']);
        let e1 = this.getAmPm(this.timings[w]['evening']['open']) + ' - ' + this.getAmPm(this.timings[w]['evening']['close']);
        temp.push({ "day": this.weekdaysFull[i++], timings: m1 + '<br/>' + e1 })
      }
      return temp;
    }
    return [];
  }

  get todayTimings() {
    if (this.timings) {
      let temp = [];
      let today = moment();
      let todayWeekDayName = today.format('dddd').substr(0, 3);
      let todaysTimings = this.timings[todayWeekDayName];
      let m1 = this.getAmPm(todaysTimings['morning']['open']) + ' - ' + this.getAmPm(todaysTimings['morning']['close']);
      let e1 = this.getAmPm(todaysTimings['evening']['open']) + ' - ' + this.getAmPm(todaysTimings['evening']['close']);
      let status = 'Closed';

      let morning_open_time = moment(todaysTimings['morning']['open'], "HH:mm");
      let morning_close_time = moment(todaysTimings['morning']['close'], "HH:mm");
      let evening_open_time = moment(todaysTimings['evening']['open'], "HH:mm");
      let evening_close_time = moment(todaysTimings['evening']['close'], "HH:mm");
      let currentTime = moment(today.format('HH:mm'), 'HH:mm');

      if (currentTime.isBetween(morning_open_time, morning_close_time)) {
        status = "Open";
      } else if (currentTime.isBetween(evening_open_time, evening_close_time)) {
        status = "Open";
      }

      temp.push({ day: "Today", timings: m1 + '<br/>' + e1, status: status })
      return temp;
    }
    return [];
  }

  getAmPm(timeString) {
    return moment(timeString, "HH:mm").format('hh:mm a');
  }

  fff = [{
    day: 'ddd',
    timings: 'ffff'
  }, {
    day: 'ddd',
    timings: 'ffff'
  }]
}
