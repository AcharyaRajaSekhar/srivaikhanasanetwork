import { Component, Input } from '@angular/core';
import { ViewController } from 'ionic-angular';
import moment from 'moment';
import { PostsProvider } from '../../../providers/posts/posts';
import { ToastMessageService, BusyIndicatorService } from '@sri-vaikhanasa-network/ion-platform-services-library';
@Component({
  selector: 'edit-open-timings',
  templateUrl: 'edit-open-timings.html'
})
export class EditOpenTimingsComponent {
  
  @Input() temple: any = {};

  weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  _sameForAllDays: boolean = false;
  get sameForAllDays() { return this._sameForAllDays; }
  set sameForAllDays(value) {
    this._sameForAllDays = value;
    this.onTimeChange();
  }
  isDisabled(i) {
    if (this.sameForAllDays && i > 0) { return true; }
    return false;
  }
  isLabelDisabled(i) {
    if (this.sameForAllDays && i > 0) { return 'weekday-disabled'; }
    return 'weekday';
  }

  data: any = {}

  ngOnInit() {

    if (this.temple && this.temple.timings) {
      let w: any = {};
      let m: any = {};
      let t: any = {};
      for (w in this.temple.timings) {
        for (m in this.temple.timings[w]) {
          for (t in this.temple.timings[w][m]) {
            this.data[w + '_' + m + '_' + t] = this.temple.timings[w][m][t];
          }
        }
      }
      this.sameForAllDays = this.temple.timings.sameForAllDays;
    } 
    else {
      this.sameForAllDays = true;
    }
  }

  constructor(
    private busyIndicator: BusyIndicatorService,
    private toastr: ToastMessageService,
    private viewCtrl: ViewController,
    private postSvc: PostsProvider) { }

  onTimeChange() {
    if (this.sameForAllDays) {
      for (let w of this.weekdays) {
        if (w != 'Sun') {
          this.data[w + '_morning_open'] = this.data['Sun_morning_open'];
          this.data[w + '_morning_close'] = this.data['Sun_morning_close'];
          this.data[w + '_evening_open'] = this.data['Sun_evening_open'];
          this.data[w + '_evening_close'] = this.data['Sun_evening_close'];
        }
      }
    }
  }

  tringTringValidation() {
    for (let w of this.weekdays) {
      let morning_open_time = moment(this.data[w + '_morning_open'], "HH:mm");
      let morning_close_time = moment(this.data[w + '_morning_close'], "HH:mm");
      let evening_open_time = moment(this.data[w + '_evening_open'], "HH:mm");
      let evening_close_time = moment(this.data[w + '_evening_close'], "HH:mm");

      if (evening_close_time.isBefore(evening_open_time)) {
        this.data[w + '_evening_close'] = this.data[w + '_evening_open'];
      }
      if (evening_open_time.isBefore(morning_close_time)) {
        this.data[w + '_evening_open'] = this.data[w + '_morning_close'];
      }
      if (morning_close_time.isBefore(morning_open_time)) {
        this.data[w + '_morning_close'] = this.data[w + '_morning_open'];
      }

      morning_open_time = moment(this.data[w + '_morning_open'], "HH:mm");
      morning_close_time = moment(this.data[w + '_morning_close'], "HH:mm");
      evening_open_time = moment(this.data[w + '_evening_open'], "HH:mm");
      evening_close_time = moment(this.data[w + '_evening_close'], "HH:mm");

      if (morning_open_time.isAfter(morning_close_time)) {
        this.data[w + '_morning_open'] = this.data[w + '_morning_close'];
      }
      if (morning_close_time.isAfter(evening_open_time)) {
        this.data[w + '_morning_close'] = this.data[w + '_evening_open'];
      }
      if (evening_open_time.isAfter(evening_close_time)) {
        this.data[w + '_evening_open'] = this.data[w + '_evening_close'];
      }

      morning_open_time = moment(this.data[w + '_morning_open'], "HH:mm");
      morning_close_time = moment(this.data[w + '_morning_close'], "HH:mm");
      evening_open_time = moment(this.data[w + '_evening_open'], "HH:mm");
      evening_close_time = moment(this.data[w + '_evening_close'], "HH:mm");

      if (evening_close_time.isBefore(evening_open_time)) {
        this.data[w + '_evening_close'] = this.data[w + '_evening_open'];
      }
      if (evening_open_time.isBefore(morning_close_time)) {
        this.data[w + '_evening_open'] = this.data[w + '_morning_close'];
      }
      if (morning_close_time.isBefore(morning_open_time)) {
        this.data[w + '_morning_close'] = this.data[w + '_morning_open'];
      }

      morning_open_time = moment(this.data[w + '_morning_open'], "HH:mm");
      morning_close_time = moment(this.data[w + '_morning_close'], "HH:mm");
      evening_open_time = moment(this.data[w + '_evening_open'], "HH:mm");
      evening_close_time = moment(this.data[w + '_evening_close'], "HH:mm");

      if (morning_open_time.isAfter(morning_close_time)) {
        this.data[w + '_morning_open'] = this.data[w + '_morning_close'];
      }
      if (morning_close_time.isAfter(evening_open_time)) {
        this.data[w + '_morning_close'] = this.data[w + '_evening_open'];
      }
      if (evening_open_time.isAfter(evening_close_time)) {
        this.data[w + '_evening_open'] = this.data[w + '_evening_close'];
      }

      break;
    }
  }

  save() {
    this.busyIndicator.show();
    this.onTimeChange();
    let temp = { sameForAllDays: this.sameForAllDays };
    for (let w of this.weekdays) {
      temp[w] = {
        "morning": { "open": this.data[w + "_morning_open"], "close": this.data[w + "_morning_close"] },
        "evening": { "open": this.data[w + "_evening_open"], "close": this.data[w + "_evening_close"] }
      }
    }
    this.postSvc.patchUpdatePostById(this.temple.id, "timings", temp).then(() => {
      this.busyIndicator.hide();
      this.toastr.show("timings updated successfully...");
      this.goBack();
    }).catch(err => {
      this.busyIndicator.hide();
      this.toastr.show("Error occured while updating timings: " + JSON.stringify(err), true);
    })
  }

  goBack() {
    this.viewCtrl.dismiss();
  }
}