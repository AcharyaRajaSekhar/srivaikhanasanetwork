import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';
import { ArchakaPostBasicCardComponent } from './archaka-posts/archaka-post-basic-card/archaka-post-basic-card';
import { ArchakaPostDetailedCardComponent } from './archaka-posts/archaka-post-detailed-card/archaka-post-detailed-card';
import { PipesModule } from '../pipes/pipes.module';
import { ImagePickerComponent } from './gallery/image-picker/image-picker';
import { ArchakaPostListComponent } from './archaka-posts/archaka-post-list/archaka-post-list';
import { TempleListComponent } from './temples/temple-list/temple-list';
import { TempleBasicCardComponent } from './temples/temple-basic-card/temple-basic-card';
import { EditOpenTimingsComponent } from './open-timings/edit-open-timings/edit-open-timings';
import { OpenTimingsComponent } from './open-timings/open-timings';
import { TempleContextMenuComponent } from './temples/temple-context-menu/temple-context-menu';
import { CalendarComponent } from './calendar/calendar';
import { AddCalendarEventComponent } from './calendar/add-calendar-event/add-calendar-event';
import { AccordionComponent } from './accordion/accordion';
import { GalleryComponent } from './gallery/gallery';
import { GoogleMapsComponent } from './google-maps/google-maps';

import { SkRotateplaneSpinnerComponent } from './spinners/sk-rotateplane-spinner/sk-rotateplane-spinner';
import { SkDoubleBounce2SpinnerComponent } from './spinners/sk-double-bounce2-spinner/sk-double-bounce2-spinner';
import { SkStretchdelaySpinnerComponent } from './spinners/sk-stretchdelay-spinner/sk-stretchdelay-spinner';
import { SkCubemoveSpinnerComponent } from './spinners/sk-cubemove-spinner/sk-cubemove-spinner';
import { SkScaleoutSpinnerComponent } from './spinners/sk-scaleout-spinner/sk-scaleout-spinner';
import { SkRotateSpinnerComponent } from './spinners/sk-rotate-spinner/sk-rotate-spinner';
import { SkBounce1SpinnerComponent } from './spinners/sk-bounce1-spinner/sk-bounce1-spinner';
import { SkCircleSpinnerComponent } from './spinners/sk-circle-spinner/sk-circle-spinner';
import { SkCubeGridSpinnerComponent } from './spinners/sk-cube-grid-spinner/sk-cube-grid-spinner';
import { SkFadingCircleSpinnerComponent } from './spinners/sk-fading-circle-spinner/sk-fading-circle-spinner';
import { SkFoldingCubeSpinnerComponent } from './spinners/sk-folding-cube-spinner/sk-folding-cube-spinner';
import { OwnerComponent } from './owner/owner';
import { SvnModal80by80 } from './platform/SvnModal80by80';
import { SvnModal100by60 } from './platform/SvnModal100by60';
import { SvnAvgPopupModal } from './platform/SvnAvgPopupModal';
import { CoverPhotoSlidesComponent } from './cover-photo-slides/cover-photo-slides';

@NgModule({
    declarations: [
        ArchakaPostBasicCardComponent,
        ArchakaPostDetailedCardComponent,
        ImagePickerComponent,
        ArchakaPostListComponent,
        TempleListComponent,
        TempleBasicCardComponent,
        EditOpenTimingsComponent,
        OpenTimingsComponent,
        TempleContextMenuComponent,
        CalendarComponent,
        AddCalendarEventComponent,
        AccordionComponent,
        GalleryComponent,
        GoogleMapsComponent,
        
        SkRotateplaneSpinnerComponent,
        SkDoubleBounce2SpinnerComponent,
        SkStretchdelaySpinnerComponent,
        SkCubemoveSpinnerComponent,
        SkScaleoutSpinnerComponent,
        SkRotateSpinnerComponent,
        SkBounce1SpinnerComponent,
        SkCircleSpinnerComponent,
        SkCubeGridSpinnerComponent,
        SkFadingCircleSpinnerComponent,
        SkFoldingCubeSpinnerComponent,
        OwnerComponent,
        SvnModal80by80,
        SvnModal100by60,
        SvnAvgPopupModal,
        CoverPhotoSlidesComponent,
    ],
    imports: [
        IonicModule,
        PipesModule,
        CommonModule
    ],
    exports: [
        ArchakaPostBasicCardComponent,
        ArchakaPostDetailedCardComponent,
        ImagePickerComponent,
        ArchakaPostListComponent,
        TempleListComponent,
        TempleBasicCardComponent,
        EditOpenTimingsComponent,
        OpenTimingsComponent,
        TempleContextMenuComponent,
        CalendarComponent,
        AddCalendarEventComponent,
        AccordionComponent,
        GalleryComponent,
        GoogleMapsComponent,
        
        SkRotateplaneSpinnerComponent,
        SkDoubleBounce2SpinnerComponent,
        SkStretchdelaySpinnerComponent,
        SkCubemoveSpinnerComponent,
        SkScaleoutSpinnerComponent,
        SkRotateSpinnerComponent,
        SkBounce1SpinnerComponent,
        SkCircleSpinnerComponent,
        SkCubeGridSpinnerComponent,
        SkFadingCircleSpinnerComponent,
        SkFoldingCubeSpinnerComponent,
        OwnerComponent,
        SvnModal80by80,
        SvnModal100by60,
        SvnAvgPopupModal,
        CoverPhotoSlidesComponent,
    ],
    entryComponents: [
        ArchakaPostBasicCardComponent,
        ArchakaPostDetailedCardComponent,
        ImagePickerComponent,
        ArchakaPostListComponent,
        TempleListComponent,
        TempleBasicCardComponent,
        EditOpenTimingsComponent,
        OpenTimingsComponent,
        TempleContextMenuComponent,
        CalendarComponent,
        AddCalendarEventComponent,
        AccordionComponent,
        GalleryComponent,
        GoogleMapsComponent,
        
        SkRotateplaneSpinnerComponent,
        SkDoubleBounce2SpinnerComponent,
        SkStretchdelaySpinnerComponent,
        SkCubemoveSpinnerComponent,
        SkScaleoutSpinnerComponent,
        SkRotateSpinnerComponent,
        SkBounce1SpinnerComponent,
        SkCircleSpinnerComponent,
        SkCubeGridSpinnerComponent,
        SkFadingCircleSpinnerComponent,
        SkFoldingCubeSpinnerComponent,
        OwnerComponent,
        SvnModal80by80,
        SvnModal100by60,
        SvnAvgPopupModal,
        CoverPhotoSlidesComponent,
    ]
})
export class ComponentsModule { }
