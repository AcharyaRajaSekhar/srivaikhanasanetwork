import { Component, Input } from '@angular/core';

@Component({
  selector: 'temple-list',
  templateUrl: 'temple-list.html'
})
export class TempleListComponent {
  @Input() posts: any;  
}
