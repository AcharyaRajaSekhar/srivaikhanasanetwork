import { Component } from '@angular/core';
import { ViewController, ModalController } from 'ionic-angular';
import { EditOpenTimingsComponent } from '../../open-timings/edit-open-timings/edit-open-timings';

@Component({
  selector: 'temple-context-menu',
  templateUrl: 'temple-context-menu.html'
})
export class TempleContextMenuComponent {

  temple: any = {};

  constructor(
    private viewCtrl: ViewController,
    private modalCtrl: ModalController) {
      this.temple = this.viewCtrl.data;
  }

  close() {
    this.viewCtrl.dismiss();
  }

  openEditTimings() {
    let m = this.modalCtrl.create(EditOpenTimingsComponent, this.temple);
    m.present();
    this.close();
  }

}
