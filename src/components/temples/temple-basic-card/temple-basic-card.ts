import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AddressApiProvider } from '../../../providers/address-api/address-api';

@Component({
  selector: 'temple-basic-card',
  templateUrl: 'temple-basic-card.html'
})
export class TempleBasicCardComponent {
  @Input() post: any;

  constructor(private navCtrl: NavController, private addressApi: AddressApiProvider) { }

  getCoverPhoto(post) {
    if (post && post.photos && post.photos.length > 0) {
      return post.photos[0];
    }
    return 'assets/imgs/defaults/1.png';
  }

  getSimpleAddress() {
    return this.addressApi.getSimpleAddress(this.post.address);
  }

  openDetails() {
    this.navCtrl.push('TempleProfilePage', this.post);
  }
}
