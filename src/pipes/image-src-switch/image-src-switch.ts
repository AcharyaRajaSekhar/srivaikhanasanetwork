import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imageSrcSwitch',
})
export class ImageSrcSwitchPipe implements PipeTransform {
  transform(src: string) {
    if (src.includes('staticflickr.com')) {
      return src.slice(0, -6) + '_q.jpg';
    }
    return src;
  }
}
