import { DatePipe } from '@angular/common';
import { Inject, LOCALE_ID, Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'date2string',
})
export class DateToStringPipe implements PipeTransform {
  constructor(@Inject(LOCALE_ID) private locale: string, private datePipe: DatePipe) {
  }

  transform(value: Date, format?: string): string {
    if (value) {
      return this.datePipe.transform(value, format || "MMM d ''yy h:mm a", this.locale);
    }
    else { return null; }
  }
}
