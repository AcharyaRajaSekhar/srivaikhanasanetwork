import { NgModule } from '@angular/core';
import { DocPipe } from './doc/doc';
import { TimeAgoPipe } from 'time-ago-pipe';
import { DateToStringPipe } from './date-to-string/date-to-string';
import { TimestampToDatePipe } from './timestamp-to-date/timestamp-to-date';
import { AmIOwnerPipe } from './am-i-owner/am-i-owner';
import { ImageSrcSwitchPipe } from './image-src-switch/image-src-switch';
import { UserProfilePicPipe } from './user-profile-pic/user-profile-pic';

@NgModule({
	declarations: [
		TimeAgoPipe,
		DocPipe,
		DateToStringPipe,
		TimestampToDatePipe,
    AmIOwnerPipe,
    ImageSrcSwitchPipe,
    UserProfilePicPipe
	],
	imports: [],
	exports: [
		TimeAgoPipe,
		DocPipe,
		DateToStringPipe,
		TimestampToDatePipe,
    AmIOwnerPipe,
    ImageSrcSwitchPipe,
    UserProfilePicPipe
	]
})
export class PipesModule { }
