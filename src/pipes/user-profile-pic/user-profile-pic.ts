import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userProfilePic',
})
export class UserProfilePicPipe implements PipeTransform {
  transform(value: string) {
    if (value) {
      if (value.includes('staticflickr.com')) {
        return value.slice(0, -6) + '_q.jpg';
      }
      return value;
    }
    return 'assets/imgs/defaults/2.png';
  }
}
