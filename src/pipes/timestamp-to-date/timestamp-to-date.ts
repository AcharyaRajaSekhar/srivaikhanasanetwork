import { Pipe, PipeTransform } from '@angular/core';
import { firestore } from 'firebase/app';
import Timestamp = firestore.Timestamp;
@Pipe({
  name: 'ts2date',
})
export class TimestampToDatePipe implements PipeTransform {
  
  constructor() { }

  transform(value: Timestamp): Date {
    if (value) { return value.toDate(); }
  }
}
