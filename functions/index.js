'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
admin.firestore().settings({ timestampsInSnapshots: true });
const express = require('express');

const app = express();

require('./middleware')(app);
require('./endpoints')(app);

// This HTTPS endpoint can only be accessed by your Firebase Users.
// Requests need to be authorized by providing an `Authorization` HTTP header
// with value `Bearer <Firebase ID Token>`.
const api = functions.https.onRequest(app);
const trigger = require('./trigger');
module.exports = { api, trigger }

