const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();

module.exports = functions.auth.user().onCreate(user => {
    var userProfile = {
        id: user.uid,
        name: user.displayName || 'User Name',
        email: user.email || 'Not available',
        gothram: 'Gothram',
        surname: 'Surname',
        photoURL: user.photoURL,
        createdOn: new Date(),
        phoneNumber: user.phoneNumber,
        address: {}
    };
    console.log(userProfile);
    return db.collection('users').doc(user.uid).set(userProfile);
});