const functions = require('firebase-functions');
const admin = require('firebase-admin');
const moment = require('moment');

module.exports = functions.firestore
    .document('gallery/{docId}')
    .onDelete((change) => {
        return new Promise((resolve, reject) => {
            let g = change.data();
            if (g.photos) {
                let reqPromises = [];
                let photoImageFileURLs = [];
                g.photos.forEach(p => {
                    if (p && p.includes('sri-vaikhanasa-net.appspot.com')) {
                        let fileId = p.substring(p.lastIndexOf('photos%2F') + 'photos%2F'.length, p.lastIndexOf('?alt=media'));
                        photoImageFileURLs.push(fileId);
                    }
                })
                photoImageFileURLs.forEach(fileId => {
                    console.log("Deleting file: " + fileId);
                    reqPromises.push(admin.storage().bucket().file('photos/' + fileId).delete());
                })
                Promise.all(reqPromises).then(() => {
                    console.log("End of delete files task @ " + moment().utcOffset("+05:30").format('YYYY-MM-DD hh:mm:ss a'));
                    resolve();
                }).catch(err => reject(err));
            }
            else resolve();
        });
    });