const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();

module.exports.ongallerycreate = functions.firestore
    .document('gallery/{docId}')
    .onCreate((change) => {
        const document = change.exists ? change.data() : null;
        if (document &&
            document.parentType === "user-profile-photo" &&
            document.photos &&
            document.photos.length === 1) {
            console.log("Updating user " + document.parentId + " profile photURL: " + document.photos[0])
            return db.doc('users/' + document.parentId).update({ 'photoURL': document.photos[0] });
        }
        else return true;
    });

module.exports.ongalleryupdate = functions.firestore
    .document('gallery/{docId}')
    .onUpdate((change) => {
        const document = change.after.exists ? change.after.data() : null;
        if (!document) return true;
        if (document &&
            document.parentType === "user-profile-photo" &&
            document.photos &&
            document.photos.length === 1) {
            console.log("Updating user " + document.parentId + " profile photURL: " + document.photos[0])
            return db.doc('users/' + document.parentId).update({ 'photoURL': document.photos[0] });
        }
        else return true;
    });