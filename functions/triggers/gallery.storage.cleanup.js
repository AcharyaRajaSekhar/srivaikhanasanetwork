const functions = require('firebase-functions');
const admin = require('firebase-admin');
const moment = require('moment');

module.exports = functions.firestore
    .document('gallery/{docId}')
    .onUpdate((change) => {
        const newDocument = change.after.exists ? change.after.data() : null;
        if (!newDocument) return true;
        const oldDocument = change.before.exists ? change.before.data() : null;
        if (!oldDocument) return true;

        if (oldDocument.photos && oldDocument.photos.length > 0) {
            if (newDocument.photos && newDocument.photos.length > 0) {
                return new Promise((resolve, reject) => {
                    let reqPromises = [];
                    let photoImageFileURLs = [];
                    oldDocument.photos.forEach(op => {
                        if (!newDocument.photos.includes(op)) {
                            if (op && op.includes('sri-vaikhanasa-net.appspot.com')) {
                                let fileId = op.substring(op.lastIndexOf('photos%2F') + 'photos%2F'.length, op.lastIndexOf('?alt=media'));
                                photoImageFileURLs.push(fileId);
                            }
                        }
                    })
                    photoImageFileURLs.forEach(fileId => {
                        console.log("Deleting file: " + fileId);
                        reqPromises.push(admin.storage().bucket().file('photos/' + fileId).delete());
                    })
                    Promise.all(reqPromises).then(() => {
                        console.log("End of delete files task @ " + moment().utcOffset("+05:30").format('YYYY-MM-DD hh:mm:ss a'));
                        resolve();
                    }).catch(err => reject(err));
                });
            }
            else {
                return new Promise((resolve, reject) => {
                    let reqPromises = [];
                    let photoImageFileURLs = [];
                    oldDocument.photos.forEach(p => {
                        if (p && p.includes('sri-vaikhanasa-net.appspot.com')) {
                            let fileId = p.substring(p.lastIndexOf('photos%2F') + 'photos%2F'.length, p.lastIndexOf('?alt=media'));
                            photoImageFileURLs.push(fileId);
                        }
                    })
                    photoImageFileURLs.forEach(fileId => {
                        console.log("Deleting file: " + fileId);
                        reqPromises.push(admin.storage().bucket().file('photos/' + fileId).delete());
                    })
                    Promise.all(reqPromises).then(() => {
                        console.log("End of delete files task @ " + moment().utcOffset("+05:30").format('YYYY-MM-DD hh:mm:ss a'));
                        resolve();
                    }).catch(err => reject(err));
                });
            }
        }
    });