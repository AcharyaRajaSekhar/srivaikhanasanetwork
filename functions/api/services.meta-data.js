'use strict';

const admin = require('firebase-admin');
const db = admin.firestore();

module.exports.get = (req, res) => {
    db.doc('metadata/servicesconfig').get().then(doc => {
        res.json(doc.data())
    })
}

module.exports.set = (req, res) => {
    console.log(req.body);
    db.doc('metadata/servicesconfig').set(req.body).then(val => {
        res.json(val);
    })
}