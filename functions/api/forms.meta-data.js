'use strict';

const admin = require('firebase-admin');
const db = admin.firestore();

module.exports.get = (req, res) => {
    db.collection('metadata/formconfig/forms').doc(req.params.formId).get().then(doc => {
        res.json(doc.data())
    })
}

module.exports.set = (req, res) => {
    console.log(req.body);
    db.collection('metadata/formconfig/forms').doc(req.params.formId).set(req.body).then(val => {
        res.json(val);
    })
}