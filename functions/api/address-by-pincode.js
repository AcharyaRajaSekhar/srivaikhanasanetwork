'use strict';

const request = require('request');

module.exports = (req, res) => {
    const url = 'http://postalpincode.in/api/pincode/' + req.params.pincode;
    // req.pipe(request.get(url)).pipe(res);
    request(url, { json: true }, (error, response, body) => {
        if (!error && response.statusCode == 200) {
            return res.json(body);
        } else {
            console.log('Error: ' + JSON.stringify(error));
            return res.json({ message: "Error"});
        }
    });
};