'use strict'

const addressByPincode = require('./api/address-by-pincode');
const helloWorld = require('./api/hello-world');
const formConfig = require('./api/forms.meta-data');
const servicesConfig = require('./api/services.meta-data');

module.exports = (app) => {
    app.get('/', helloWorld);
    app.get('/address-by-pincode/:pincode', addressByPincode);

    app.get('/form-config/:formId', formConfig.get);
    app.post('/form-config/:formId', formConfig.set);

    app.get('/service-config/services', servicesConfig.get);
    app.post('/service-config/services', servicesConfig.set);
}