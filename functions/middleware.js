'use strict'

const cookieParser = require('cookie-parser')();
const cors = require('cors')({
  origin: true,
  allowedHeaders: ['Content-Type', 'Authorization', 'Content-Length', 'X-Requested-With', 'Accept'],
  methods: ['OPTIONS', 'GET', 'PUT', 'POST', 'DELETE'],
  'preflightContinue': false
});
const bodyParser = require('body-parser');

const validateFirebaseIdToken = require('./middlewares/validate-firebase-id-token');

module.exports = (app) => {
  app.use(cors);
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(cookieParser);
  // app.use(validateFirebaseIdToken);
}