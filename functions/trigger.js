
module.exports.userprofilephotochange = require('./triggers/user.profile.photo.change');
module.exports.onusersignup = require('./triggers/user.auth.signup');
module.exports.ongalleryupdate = require('./triggers/gallery.storage.cleanup');
module.exports.ongallerydelete = require('./triggers/gallery.item.delete');